package bytesare.us.components;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.PackageResourceReference;

import java.util.regex.Pattern;

/**
 * Created by stefan on 17.02.16.
 */
public class ControlPanel extends Panel {
    public static final Pattern PLAY = Pattern.compile("PLAY\\.(\\d)");
    public static final Pattern STOP = Pattern.compile("STOP\\.(\\d)");
    public static final Pattern PAUSE = Pattern.compile("PAUSE\\.(\\d)");
    public static final Pattern FASTER = Pattern.compile("FASTER\\.(\\d)");
    public static final Pattern SLOWER = Pattern.compile("SLOWER\\.(\\d)");

    public ControlPanel(String id) {
        super(id);
        add(JavascriptLinkFactory.createWebSocketRunButton("play", new PackageResourceReference(getClass(), "play.svg"), JavascriptLinkFactory.COMMANDS.PLAY));
        add(JavascriptLinkFactory.createWebSocketRunButton("pause", new PackageResourceReference(getClass(), "pause.svg"), JavascriptLinkFactory.COMMANDS.PAUSE));
        add(JavascriptLinkFactory.createWebSocketRunButton("stop", new PackageResourceReference(getClass(), "stop.svg"), JavascriptLinkFactory.COMMANDS.STOP));
        add(JavascriptLinkFactory.createWebSocketRunButton("faster", new PackageResourceReference(getClass(), "faster.svg"), JavascriptLinkFactory.COMMANDS.FASTER));
        add(JavascriptLinkFactory.createWebSocketRunButton("slower", new PackageResourceReference(getClass(), "slower.svg"), JavascriptLinkFactory.COMMANDS.SLOWER));
    }


}
