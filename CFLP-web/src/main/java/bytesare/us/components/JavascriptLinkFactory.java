package bytesare.us.components;

import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.request.resource.ResourceReference;

/**
 * Created by stefan on 05.02.16.
 */
public class JavascriptLinkFactory {

    private JavascriptLinkFactory() {
    }

    public static Image createWebSocketRunButton(String id, ResourceReference resourceReference, COMMANDS command) {
        return new Image(id, resourceReference) {
            @Override
            protected void onInitialize() {
                super.onInitialize();
                add(new AttributeAppender("onclick", command.getJs()));
            }
        };
    }

    protected enum COMMANDS {
        PLAY(new StringBuilder().append("Wicket.WebSocket.send('PLAY.'").append("+ $('#instances').val()").append("); return false;").toString()),
        STOP(new StringBuilder().append("Wicket.WebSocket.send('STOP.'").append("+ $('#instances').val()").append("); return false;").toString()),
        PAUSE(new StringBuilder().append("Wicket.WebSocket.send('PAUSE.'").append("+ $('#instances').val()").append("); return false;").toString()),
        FASTER(new StringBuilder().append("Wicket.WebSocket.send('FASTER.'").append("+ $('#instances').val()").append("); return false;").toString()),
        SLOWER(new StringBuilder().append("Wicket.WebSocket.send('SLOWER.'").append("+ $('#instances').val()").append("); return false;").toString());

        private final String js;

        COMMANDS(String js) {
            this.js = js;
        }

        public String getJs() {
            return js;
        }
    }
}