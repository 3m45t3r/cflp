package bytesare.us;

import bytesare.us.client.CFLPClient;
import bytesare.us.client.WebSocketConnector;
import bytesare.us.pages.HomePage;
import bytesare.us.pages.PageResources;
import org.apache.log4j.Logger;
import org.apache.wicket.Application;
import org.apache.wicket.Session;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.ws.api.IWebSocketConnection;
import org.apache.wicket.protocol.ws.api.registry.SimpleWebSocketConnectionRegistry;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import static java.util.stream.Collectors.toList;

/**
 * Application object for your web application.
 * thanks to andre
 */
public class WicketApplication extends WebApplication {

    private static final Logger LOG = Logger.getLogger(WicketApplication.class);
    private final ExecutorService executorService;
    volatile boolean applicationDestroyCalled = false;
    private Map<String, CFLPClient> clientsBySessionId = new LinkedHashMap<>();


    public WicketApplication() {
        executorService = Executors.newCachedThreadPool();
    }

    public static WicketApplication get() {
        return (WicketApplication) Application.get();
    }

    /**
     * @see org.apache.wicket.Application#getHomePage()
     */
    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
        super.init();
        getMarkupSettings().setStripWicketTags(true);
        //getMarkupSettings().setCompressWhitespace(true);
        //getMarkupSettings().setStripComments(true);

//        mountPage("/cflp", HomePage.class);
        PageResources.registerBootstrapResources(this);
    }

    public CFLPClient createNewClient(int pageId) {
        Session session = Session.get();
        if (session.isTemporary() || session.getId() == null) {
            session.bind();
        }
        String sessionId = session.getId();
        WebSocketConnector connector = new WebSocketConnector(this, sessionId, pageId);
        CFLPClient client = new CFLPClient(connector, new LinkedBlockingQueue(2048));
        clientsBySessionId.put(sessionId, client);
        LOG.debug("a new client was created. " + sessionId);
        return client;
    }

    public void removeClient(String sessionId) {
        clientsBySessionId.remove(sessionId);
    }

    public CFLPClient getExistingClient(int pageId) {
        Session session = Session.get();
        if (session.isTemporary() || session.getId() == null) {
            return null;
        }
        CFLPClient client = clientsBySessionId.get(session.getId());
        if (client == null) {
            return null;
        }
        client.getConnector().setPageIdKey(pageId);
        return client;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    @SuppressWarnings("Duplicates")
    @Override
    protected void onDestroy() {
        applicationDestroyCalled = true;

        LOG.info("Closing all open webSocket connections...");
        List<IWebSocketConnection> openConnections = new SimpleWebSocketConnectionRegistry()
                .getConnections(this)
                .stream().filter(x -> x != null && x.isOpen())
                .collect(toList());
        openConnections.forEach(x -> x.close(500, "game application shutting down."));
        LOG.info("All open webSocket connections were closed.");

        LOG.info("Stopping all cflp threads...");
        executorService.shutdownNow();
        LOG.info("All cflp threads were stopped.");

        clientsBySessionId.clear();
    }


}
