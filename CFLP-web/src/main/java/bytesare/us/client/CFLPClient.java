package bytesare.us.client;

import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by stefan on 28.01.16.
 */
public class CFLPClient {
    private final WebSocketConnector connector;
    private final LinkedBlockingQueue<String> queue;
    private CompletableFuture<String> futureResult;

    public CFLPClient(WebSocketConnector connector, LinkedBlockingQueue<String> queue) {
        this.connector = connector;
        this.queue = queue;
    }

    public WebSocketConnector getConnector() {
        return connector;
    }

    public void requestInstance() {

    }

    public void startInstanceAndWatch(Integer instanceId) {
    }

    public LinkedBlockingQueue getQueue() {
        return queue;
    }

    public CompletableFuture<String> getFutureResult() {
        return futureResult;
    }

    public void setFutureResult(CompletableFuture<String> futureResult) {
        this.futureResult = futureResult;
    }

    public void run() throws InterruptedException, ExecutionException {
        if (queue.isEmpty()) {
            Thread.sleep(250);
        }
        while (!queue.isEmpty()) {
            connector.sendMessage(new AborJsPushMessage(queue.take()));
        }
        if (futureResult.isDone()) {
//            connector.sendMessage(new AborJsPushMessage(futureResult.get()));
        }
    }

    public interface IEventPushMessage extends IWebSocketPushMessage {
        List<IEventPushMessage> getMessages();
    }

    public static class AborJsPushMessage implements IEventPushMessage {
        private String json;

        public AborJsPushMessage(String text) {
            this.json = text;
        }

        public List<IEventPushMessage> getMessages() {
            return Collections.singletonList(this);
        }

        public String getJson() {
            return json;
        }

        public String toString() {
            return json;
        }
    }

}
