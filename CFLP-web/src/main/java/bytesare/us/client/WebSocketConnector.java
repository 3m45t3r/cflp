package bytesare.us.client;

import org.apache.log4j.Logger;
import org.apache.wicket.Application;
import org.apache.wicket.protocol.ws.api.IWebSocketConnection;
import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;
import org.apache.wicket.protocol.ws.api.registry.IWebSocketConnectionRegistry;
import org.apache.wicket.protocol.ws.api.registry.PageIdKey;
import org.apache.wicket.protocol.ws.api.registry.SimpleWebSocketConnectionRegistry;

/**
 * encapsulates the part of the wicketPlayerClient that involves the websocket connection handling
 *
 * @author andre on 6/27/15.
 */
public class WebSocketConnector {

    private static final Logger logger = Logger.getLogger(WebSocketConnector.class);
    // the wicket application that manages all webSocket connections
    private final Application app;
    // the id of the player's webSession
    private final String sessionId;
    // the latest pageIdKey that connected to the session
    private PageIdKey pageIdKey;
    // whether the client is connected to the webSocket
    private volatile boolean webSocketConnected = false;

    public WebSocketConnector(Application app, String sessionId, int pageIdKey) {
        this.app = app;
        this.sessionId = sessionId;
        setPageIdKey(pageIdKey);
    }

    public void sendMessage(IWebSocketPushMessage m) {
        getConnection().sendMessage(m);
    }

    public boolean isWebSocketConnected() {
        return webSocketConnected;
    }

    public void setWebSocketConnected(boolean webSocketConnected) {
        this.webSocketConnected = webSocketConnected;
    }

    public String getSessionId() {
        return sessionId;
    }

    public PageIdKey getPageIdKey() {
        return pageIdKey;
    }

    public void setPageIdKey(int pageIdKey) {
        if (logger.isDebugEnabled()) {
            if (this.pageIdKey != null && this.pageIdKey.hashCode() != pageIdKey) {
                logger.info("The pageIdKey has been changed to " + pageIdKey);
            }
        }
        this.pageIdKey = new PageIdKey(pageIdKey);
    }

    private IWebSocketConnection getConnection() {
        String descr = "key: " + pageIdKey.hashCode() + ", session: " + sessionId;

        IWebSocketConnectionRegistry registry = new SimpleWebSocketConnectionRegistry();
        IWebSocketConnection connection = registry.getConnection(app, sessionId, pageIdKey);

        if (connection != null && connection.isOpen()) {
            logger.debug("Using open connection: " + descr);
            return connection;
        } else {
            throw new WebSocketConnectorException("No open connection: " + descr);
        }
    }

    public static class WebSocketConnectorException extends RuntimeException {
        public WebSocketConnectorException(String message) {
            super(message);
        }
    }
}
