package bytesare.us.pages;

import bytesare.us.WicketApplication;
import bytesare.us.cflp.core.AlgorithmExecutor;
import bytesare.us.client.CFLPClient;
import bytesare.us.components.ControlPanel;
import org.apache.log4j.Logger;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.ajax.WicketEventJQueryResourceReference;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptReferenceHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.protocol.ws.api.WebSocketBehavior;
import org.apache.wicket.protocol.ws.api.WebSocketRequestHandler;
import org.apache.wicket.protocol.ws.api.WicketWebSocketJQueryResourceReference;
import org.apache.wicket.protocol.ws.api.message.ClosedMessage;
import org.apache.wicket.protocol.ws.api.message.ConnectedMessage;
import org.apache.wicket.protocol.ws.api.message.IWebSocketPushMessage;
import org.apache.wicket.protocol.ws.api.message.TextMessage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.ResourceReference;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;

import static java.util.Collections.singletonList;
import static org.apache.wicket.markup.head.JavaScriptHeaderItem.forReference;

@SuppressWarnings("Duplicates")
public class HomePage extends WebPage {
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = Logger.getLogger(HomePage.class);
    @SuppressWarnings("unused")
    private static final JavaScriptReferenceHeaderItem JAVA_SCRIPT_REFERENCE_HEADER_ITEM;

    private IModel<CFLPClient> clientModel = new LoadableDetachableModel<CFLPClient>() {
        @Override
        protected CFLPClient load() {
            WicketApplication app = WicketApplication.get();
            return app.getExistingClient(getPageId());
        }
    };


    public HomePage(final PageParameters parameters) {
        super(parameters);
        WicketApplication wa = WicketApplication.get();
        final CFLPClient client = wa.createNewClient(getPageId());
        clientModel.setObject(client);
        add(new WebSocketBehavior() {
            @Override
            protected void onPush(WebSocketRequestHandler handler, IWebSocketPushMessage message) {
                if (message instanceof CFLPClient.AborJsPushMessage) {
                    CFLPClient.AborJsPushMessage aborMsg = (CFLPClient.AborJsPushMessage) message;
//                    handler.appendJavaScript("sys.graft(" + aborMsg.getJson() + ")");
                    String finalMsg = aborMsg.getJson();
                    LOG.debug("Pushing message: " + finalMsg);
                    handler.push(finalMsg);
                    try {
                        Thread.sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            protected void onMessage(WebSocketRequestHandler handler, TextMessage message) {
                String text = message.getText();
                if (text == null) {
                    LOG.warn("a null message was received!");
                    return;
                }
                LOG.debug("message received from client page: " + text);
                Matcher play = ControlPanel.PLAY.matcher(text);
                if (play.matches()) {
                    Integer instanceId = Integer.parseInt(play.group(1));
                    CompletableFuture<String> algoResult = AlgorithmExecutor.solve(instanceId, getClient().getQueue(), WicketApplication.get().getExecutorService());
                    getClient().setFutureResult(algoResult);
                    try {
                        getClient().run();
                    } catch (InterruptedException | ExecutionException e) {
                        LOG.error(e.getLocalizedMessage());
                    }
                }
            }

            @Override
            protected void onConnect(ConnectedMessage message) {
                LOG.debug("connected " + message.getSessionId());
                getClient().getConnector().setWebSocketConnected(true);
            }

            @Override
            protected void onClose(ClosedMessage message) {
                LOG.debug("disconnected " + message.getSessionId());
                getClient().getConnector().setWebSocketConnected(false);
            }
        });
        add(new ControlPanel("control"));
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        PageResources.renderBootstrapResourcesToHead(response);
        PageResources.renderClassRelativeCssRef(this, response);
        PageResources.renderClassRelativeJsRef(this, response,
                WicketEventJQueryResourceReference.get(),
                WicketWebSocketJQueryResourceReference.get());
    }

    private CFLPClient getClient() {
        return clientModel.getObject();
    }

    @Override
    protected void onConfigure() {
        if (WicketApplication.get().getExistingClient(getPageId()) == null) {
            detach();
            throw new RestartResponseException(getClass());
        }
    }

    static {
        Class<? extends HomePage> clazz = HomePage.class;
        String name = clazz.getSimpleName();

        List<HeaderItem> deps = singletonList(forReference(WicketEventJQueryResourceReference.get()));
        ResourceReference jsRef = new JavaScriptResourceReference(clazz, name + ".js") {
            @Override
            public List<HeaderItem> getDependencies() {
                return deps;
            }
        };
        JAVA_SCRIPT_REFERENCE_HEADER_ITEM = forReference(jsRef);
    }


}
