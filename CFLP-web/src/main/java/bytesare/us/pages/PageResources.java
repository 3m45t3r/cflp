package bytesare.us.pages;

import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.WicketEventJQueryResourceReference;
import org.apache.wicket.markup.head.*;
import org.apache.wicket.request.resource.*;
import org.apache.wicket.resource.JQueryResourceReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.apache.wicket.markup.head.JavaScriptHeaderItem.forReference;

/**
 * thanks to andre
 */
@SuppressWarnings("Duplicates")
public class PageResources {

    public static final String BOOTSTRAP_VERSION = "3.3.5-dist";
    public static final ResourceReference BOOTSTRAP_CSS;
    public static final ResourceReference BOOTSTRAP_JS;
    public static final ResourceReference ARBOR_JS;
    public static final ResourceReference BOOTSTRAP_GLYPHICONS_WOFF;
    public static final ResourceReference BOOTSTRAP_GLYPHICONS_TTF;
    public static final HeaderItem BOOTSTRAP_CSS_HEADER_ITEM;
    public static final HeaderItem BOOTSTRAP_JS_HEADER_ITEM;
    public static final HeaderItem ARBOR_JS_HEADER_ITEM;
    private static final String ARBOR_BASE_PATH = "abor/";
    private static final String BOOTSTRAP_BASE_PATH = "bootstrap/" + BOOTSTRAP_VERSION + "/";

    private PageResources() {
    }

    public static void renderBootstrapResourcesToHead(IHeaderResponse response) {
        response.render(BOOTSTRAP_CSS_HEADER_ITEM);
        response.render(BOOTSTRAP_JS_HEADER_ITEM);
        response.render(ARBOR_JS_HEADER_ITEM);
    }

    public static void registerBootstrapResources(Application application) {
        ResourceReferenceRegistry registry = application.getResourceReferenceRegistry();
        String path = BOOTSTRAP_BASE_PATH + "fonts/glyphicons-halflings-regular.";

        for (String suffix : new String[]{"eot", "svg", "ttf", "woff", "woff2"}) {
            ResourceReference reference = new ContextRelativeResourceReference(path + suffix);
            registry.registerResourceReference(reference);
        }

        ResourceReference reference = new ContextRelativeResourceReference(BOOTSTRAP_BASE_PATH + "css/bootstrap.css.map");
        registry.registerResourceReference(reference);
    }

    public static void renderClassRelativeCssRef(Component component,
                                                 IHeaderResponse response) {
        Class<? extends Component> cls = component.getClass();
        ResourceReference cssRef = new CssResourceReference(cls, cls.getSimpleName() + ".css",
                component.getLocale(), component.getStyle(), component.getVariation());
        response.render(CssHeaderItem.forReference(cssRef));
    }

    public static void renderClassRelativeJsRef(Component component,
                                                IHeaderResponse response,
                                                JavaScriptResourceReference... jsDependencies) {
        Class<? extends Component> cls = component.getClass();
        String name = cls.getSimpleName() + ".js";

        List<HeaderItem> deps = new ArrayList<>();
        if (jsDependencies != null) {
            for (JavaScriptResourceReference depref : jsDependencies) {
                deps.add(JavaScriptHeaderItem.forReference(depref));
            }
        }

        ResourceReference jsRef = new JavaScriptResourceReference(cls, name,
                component.getLocale(), component.getStyle(), component.getVariation()) {
            @Override
            public List<HeaderItem> getDependencies() {
                return deps;
            }
        };

        response.render(JavaScriptHeaderItem.forReference(jsRef));
    }

    static {
        BOOTSTRAP_CSS = new ContextRelativeResourceReference(
                BOOTSTRAP_BASE_PATH + "css/bootstrap.css");

        final List<HeaderItem> headerItems = Collections.unmodifiableList(Arrays.asList(
                forReference(JQueryResourceReference.get()),
                forReference(WicketEventJQueryResourceReference.get())
        ));
        ARBOR_JS = new ContextRelativeResourceReference(ARBOR_BASE_PATH + "arbor.js") {
            @Override
            public List<HeaderItem> getDependencies() {
                return headerItems;
            }
        };
        BOOTSTRAP_JS = new ContextRelativeResourceReference(BOOTSTRAP_BASE_PATH + "js/bootstrap.js") {
            @Override
            public List<HeaderItem> getDependencies() {
                return headerItems;
            }
        };

        BOOTSTRAP_GLYPHICONS_WOFF = new ContextRelativeResourceReference(
                BOOTSTRAP_BASE_PATH + "fonts/glyphicons-halflings-regular.woff");
        BOOTSTRAP_GLYPHICONS_TTF = new ContextRelativeResourceReference(
                BOOTSTRAP_BASE_PATH + "fonts/glyphicons-halflings-regular.ttf");

        BOOTSTRAP_JS_HEADER_ITEM = JavaScriptReferenceHeaderItem.forReference(BOOTSTRAP_JS);
        ARBOR_JS_HEADER_ITEM = JavaScriptReferenceHeaderItem.forReference(ARBOR_JS);
        BOOTSTRAP_CSS_HEADER_ITEM = CssReferenceHeaderItem.forReference(BOOTSTRAP_CSS);
    }
}
