package bytesare.us.cflp.core;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;

/**
 * Created by stefan on 14.01.16.
 */
class CFLPJacksonRecorder {

    private static final int DEFAULT_BANDWITH = 1;
    private static final double DEFAULT_LENGTH = .5;
    private final Writer finalWriter;
    private final CFLPInstance instance;
    private final int maxCustomerDistance;
    private final JsonFactory factory;

    public CFLPJacksonRecorder(Writer writer, CFLPInstance instance, int maxCustomerDistance) {
        this.finalWriter = writer;
        this.instance = instance;
        this.maxCustomerDistance = maxCustomerDistance;
        this.factory = new JsonFactory();
        try {
            this.finalWriter.write("[");
        } catch (IOException e) {
            throw new RuntimeException("Given Writer raised an I/O exception! " + e.getLocalizedMessage());
        }
    }

    public String record(int[] mapping) {
        try {
            final Writer tmpWrite = new StringWriter();
            final JsonGenerator g = factory.createGenerator(tmpWrite);
            writeGraph(mapping, g);
            g.flush();
            String result = tmpWrite.toString();
            finalWriter.write(result + ",");
            tmpWrite.close();
            g.close();
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Failed to record mapping " + e.getLocalizedMessage());
        }

    }

    /**
     * returns an json array of all results found by the algorithm
     *
     * @return json array
     */
    public String flush() {
        //finish last recording
        String dirty = finalWriter.toString();
        return dirty.substring(0, dirty.length() - 1) + "]";
    }

    private void writeGraph(int[] mapping, JsonGenerator g) throws IOException {
        g.writeStartObject();
        createNodes(instance.getNumFacilities(), mapping.length, new SolutionMetaData(mapping), g);
        createEdges(mapping, instance.getNumFacilities(), g);
        g.writeEndObject();
    }

    private void createEdges(int[] mapping, int numFacilities, JsonGenerator g) throws IOException {
        g.writeObjectFieldStart("edges");
        for (int customer = 0; customer < mapping.length; customer++) {
            g.writeObjectFieldStart("c" + customer);
            int connectedFacility = mapping[customer];
            for (int facility = 0; facility < numFacilities; facility++) {
                if (connectedFacility == facility) {
                    g.writeObjectFieldStart("f" + facility);
                    g.writeNumberField("length", getLength(facility, customer));
                    g.writeBooleanField("connected", true);
                    g.writeNumberField("bandwidth", getBandwidth(customer));
                    g.writeEndObject();
                } else {
                    g.writeObjectFieldStart("f" + facility);
                    g.writeBooleanField("connected", false);
                    g.writeEndObject();
                }
            }
            g.writeEndObject();
        }
        g.writeEndObject();
    }

    private void createNodes(int numFacilities, int numOfCustomers, SolutionMetaData meta, JsonGenerator g) throws IOException {
        g.writeObjectFieldStart("nodes");
        for (int customer = 0; customer < numOfCustomers; customer++) {
            g.writeObjectFieldStart("c" + customer);
            g.writeBooleanField("alone", true);
            g.writeNumberField("mass", 7);
            g.writeEndObject();
        }
        for (int facility = 0; facility < numFacilities; facility++) {
            g.writeObjectFieldStart("f" + facility);
            g.writeBooleanField("alone", true);
            g.writeNumberField("mass", 23);
            g.writeStringField("bandwidthUsage", String.valueOf(meta.bandwidthLoad[facility]) + "/" + String.valueOf(instance.maxBandwidth));
            g.writeStringField("occupancyRate", String.valueOf(meta.connectedCustomers[facility]) + "/" + String.valueOf(instance.maxCustomersFor(facility)));
            g.writeEndObject();
        }
        g.writeEndObject();
    }

    private double getLength(int facility, int customer) {
        return instance != null ? BigDecimal.valueOf(instance.distance(facility, customer)).divide(BigDecimal.valueOf(maxCustomerDistance), 2).doubleValue() : DEFAULT_LENGTH;
    }

    private int getBandwidth(int customer) {
        return instance != null ? instance.bandwidthOf(customer) : DEFAULT_BANDWITH;
    }

    private class SolutionMetaData {
        public final int[] bandwidthLoad;
        public final int[] connectedCustomers;

        public SolutionMetaData(int[] mapping) {
            connectedCustomers = new int[instance.getNumFacilities()];
            bandwidthLoad = new int[instance.getNumFacilities()];
            for (int c = 0; c < mapping.length; c++) {
                int facility = mapping[c];
                connectedCustomers[facility]++;
                bandwidthLoad[facility] += instance.bandwidthOf(c);
            }
        }


    }
}
