package bytesare.us.cflp.core;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by stefan on 03.01.16.
 */
public class AlgorithmExecutor {

    private static final Map<Integer, String> INSTANCES = new HashMap<Integer, String>() {{
        put(0, "/instances/0000");
        put(1, "/instances/0001");
        put(2, "/instances/0002");
        put(3, "/instances/0003");
        put(4, "/instances/0004");
    }};

    private AlgorithmExecutor() {
    }

    public static final CompletableFuture<String> solve(Integer instance, LinkedBlockingQueue<String> queue, Executor executor) {
        CFLPInstanceReader reader = new CFLPInstanceReader(AlgorithmExecutor.class.getResourceAsStream(INSTANCES.get(instance)));
        CFLPInstance cflp = reader.readInstance();
        final AbstractCFLP bnbRunner = new CFLP(cflp, queue);
        return CompletableFuture.supplyAsync(bnbRunner, executor);
    }

    public static void main(String[] args) {
        ExecutorService e = Executors.newFixedThreadPool(1);
        LinkedBlockingQueue<String> q = new LinkedBlockingQueue<>();
        CompletableFuture<String> results = solve(0, q, e);
        results.thenApply(s -> {
            System.out.println(s);
            return s;
        });
        e.shutdown();
        try {
            System.out.println("took: " + q.take());
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
}
