package bytesare.us.cflp.core;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Klasse zum Berechnen der L&ouml;sung mittels Branch-and-Bound.
 * Hier sollen Sie Ihre L&ouml;sung implementieren.
 */
class CFLP extends AbstractLinkedBlockingQueuedCFLP {

    private static final boolean DEBUG = false;
    private static final int NOT_MAPPED = -1;
    private static final int ALL_CUSTOMERS_ASSIGNED = -1337;
    private static Timer debugTimer;
    private final CFLPInstance problemInstance;
    private final Map<Integer, List<Integer>> customerNearestFacilityMap;
    private final List<Customer> customersSortedDescByBandwith;
    private final List<Customer> customersSortedAscByBandwith;
    private final CFLPJacksonRecorder recorder;
    private int[] maxCustomerOfFacility;


    public CFLP(CFLPInstance instance,LinkedBlockingQueue<String> queue) {
        super(queue);
        this.problemInstance = instance;
        customersSortedDescByBandwith = new ArrayList<>();
        customerNearestFacilityMap = new HashMap<>();
        maxCustomerOfFacility = new int[instance.getNumFacilities()];
        int maxCustomerDistance = 0;
        for (int j = 0; j < instance.getNumFacilities(); j++) {
            maxCustomerOfFacility[j] = instance.maxCustomersFor(j);
        }
        for (int customer = 0; customer < problemInstance.getNumCustomers(); customer++) {
            final int c = customer;
            customersSortedDescByBandwith.add(new Customer(customer, problemInstance.bandwidthOf(customer)));
            List<Integer> facilitiesNearest2Customer = new ArrayList<>();
            if (!customerNearestFacilityMap.containsKey(customer)) {
                for (int facility = 0; facility < problemInstance.getNumFacilities(); facility++) {
                    facilitiesNearest2Customer.add(facility);
                    if (maxCustomerDistance < problemInstance.distance(facility, c)) {
                        maxCustomerDistance = problemInstance.distance(facility, c);
                    }
                }
                Collections.sort(facilitiesNearest2Customer, (o1, o2) -> new Integer(problemInstance.distance(o1, c))
                        .compareTo(problemInstance.distance(o2, c)));
                customerNearestFacilityMap.put(customer, facilitiesNearest2Customer);
            }
        }
        Collections.sort(customersSortedDescByBandwith, (o1, o2) -> new BigDecimal(o1.bandwithDemand)
                .compareTo(new BigDecimal(o2.bandwithDemand)));
        customersSortedAscByBandwith = new ArrayList<>(customersSortedDescByBandwith);
        Collections.reverse(customersSortedAscByBandwith);

        this.recorder = new CFLPJacksonRecorder(new StringWriter(), instance, maxCustomerDistance);
    }

    /**
     * Diese Methode bekommt vom Framework maximal 30 Sekunden Zeit zur
     * Verf&uuml;gung gestellt um eine g&uuml;ltige L&ouml;sung
     * zu finden.
     * <p/>
     * <p>
     * F&uuml;gen Sie hier Ihre Implementierung des Branch-and-Bound Algorithmus
     * ein.
     * </p>
     */
    public void run() {
        debugTimer = new Timer();
        int[] mapping = new int[problemInstance.getNumCustomers()];
        Arrays.fill(mapping, NOT_MAPPED);
        cflp(mapping);
    }

    @Override
    public String get() {
        run();
        return recorder.flush();
    }

    /**
     * Prints message if DEBUG == true
     *
     * @param msg debug msg
     */
    public void debug(String msg) {
        if (DEBUG) {
            Main.printDebug(debugTimer.printAndReset() + ": " + msg);
        }
    }

    /**
     * Prints mapping if DEBUG == true
     *
     * @param mapping given mapping
     */
    public void debug(int[] mapping) {
        if (DEBUG) {
            Main.printDebug(debugTimer.printAndReset() + ": " + Arrays.toString(mapping));
        }
    }

    /**
     * Method to determine if there are still unmapped customers left
     *
     * @param mapping given mapping
     * @return index of unmapped customer or ALL_CUSTOMERS_ASSIGNED if no unmapped customer is left
     */
    private int allCustomersAssigned(int[] mapping) {
        for (Customer c : customersSortedAscByBandwith) {
            if (mapping[c.customer] == NOT_MAPPED) {
                return c.customer;
            }
        }
        return ALL_CUSTOMERS_ASSIGNED;
    }

    /**
     * recursive method to walk through all branches and to find valid solutions.
     * A found solution is set if it is better than the current solution.
     *
     * @param mapping given mapping
     */
    private void cflp(int[] mapping) {
        if (allCustomersAssigned(mapping) == ALL_CUSTOMERS_ASSIGNED) {
            int calcValue = problemInstance.calcObjectiveValue(mapping);
            debug(mapping);
            if (getBestSolution() == null) {
                debug("FOUND");
                String jsonResult = recorder.record(mapping);
                queueAndSetSolution(calcValue, mapping, "FIRST", jsonResult);
            } else if (calcValue < getBestSolution().getUpperBound()) {
                debug("BETTER");
                String jsonResult = recorder.record(mapping);
                queueAndSetSolution(calcValue, mapping, "BETTER THAN LAST", jsonResult);
            } else {
                debug("LAME DUCK");
            }
        }
        List<Branch> branches = branch(mapping);
        for (Branch branch : branches) {
            cflp(branch.mapping);
        }
    }

    /**
     * Calculates the lower bound.
     * For an unmapped customer, add the distance to the nearest facility.
     * For a mapped customer, add the distance to the facility to which the customer is already assigned.
     * For every already assigned facility, add its opening costs.
     *
     * @param mapping given mapping
     * @return int lower bound value of mapping
     */
    private int lowerBound(int[] mapping) {
        int plus = 0;
        for (int customer = 0; customer < mapping.length; customer++) {
            if (mapping[customer] == NOT_MAPPED) {
                plus += problemInstance.distance(customerNearestFacilityMap.get(customer).get(0), customer) * problemInstance.distanceCosts;
            }
        }
        return problemInstance.calcObjectiveValue(mapping) + plus;
    }

    /**
     * Method creates for a given incomplete mapping all possible valid branches.
     *
     * @param mapping current incomplete mapping
     * @return all branches which are still inbound
     */
    private List<Branch> branch(int[] mapping) {
        List<Branch> branches = new ArrayList<Branch>();
        int unmappedCustomer = allCustomersAssigned(mapping);
        if (unmappedCustomer != ALL_CUSTOMERS_ASSIGNED) {
            for (Integer facility : customerNearestFacilityMap.get(unmappedCustomer)) {
                int[] newMapping = Arrays.copyOf(mapping, mapping.length);
                newMapping[unmappedCustomer] = facility;
                if (inBound(newMapping)) {
                    branches.add(new Branch(newMapping));
                }
            }
        }
        return branches;
    }

    /**
     * Method checks if the given newMapping does not violate constraints
     * and if the lower bound of the given mapping is smaller than the current best solution.
     *
     * @param newMapping given mapping
     * @return true if in bound, false otherwise
     */
    private boolean inBound(int[] newMapping) {
        return isValid(newMapping) && (getBestSolution() == null || lowerBound(newMapping) < getBestSolution().getUpperBound());
    }

    /**
     * Method checks if given newMapping would violate a constraint.
     *
     * @param newMapping given mapping
     * @return true if mapping violates a constraint
     */
    private boolean isValid(int[] newMapping) {
        int[] currentCustomersOfFacility = new int[problemInstance.getNumFacilities()];
        int[] currentBandwithDemandOfCustomers = new int[problemInstance.getNumFacilities()];
        for (Customer c : customersSortedDescByBandwith) {
            int facility = newMapping[c.customer];
            if (facility != NOT_MAPPED) {
                currentCustomersOfFacility[facility]++;
                currentBandwithDemandOfCustomers[facility] += problemInstance.bandwidthOf(c.customer);
                if (currentCustomersOfFacility[facility] > maxCustomerOfFacility[facility]
                        || currentBandwithDemandOfCustomers[facility] > problemInstance.maxBandwidth) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Timer for debug purposes
     */
    private static class Timer {
        private long start;

        public Timer() {
            start = System.currentTimeMillis();
        }

        public String printAndReset() {
            String ret = elapsed() + " ms";
            reset();
            return ret;
        }

        @Override
        public String toString() {
            return elapsed() + " ms";
        }

        private long elapsed() {
            return System.currentTimeMillis() - start;
        }

        private void reset() {
            start = System.currentTimeMillis();
        }
    }

    /**
     * represents a branch.
     */
    private static class Branch {
        public final int[] mapping;

        private Branch(int[] mapping) {
            this.mapping = mapping;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Branch branch = (Branch) o;

            return Arrays.equals(mapping, branch.mapping);

        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(mapping);
        }

        @Override
        public String toString() {
            return "Branch{" +
                    "mapping=" + Arrays.toString(mapping) +
                    '}';
        }
    }

    /**
     * represents a customer. it is used to sort customers by their bandwith demands
     */
    private static class Customer {
        public final int customer;
        public final int bandwithDemand;

        private Customer(int customer, int bandwithDemand) {
            this.customer = customer;
            this.bandwithDemand = bandwithDemand;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Customer customer1 = (Customer) o;

            return customer == customer1.customer && bandwithDemand == customer1.bandwithDemand;

        }

        @Override
        public int hashCode() {
            int result = customer;
            result = 31 * result + bandwithDemand;
            return result;
        }
    }
}
