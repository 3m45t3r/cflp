package bytesare.us.cflp.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by stefan on 25.01.16.
 */
abstract class AbstractLinkedBlockingQueuedCFLP extends AbstractCFLP implements Runnable {
    final Logger logger = LoggerFactory.getLogger(AbstractLinkedBlockingQueuedCFLP.class);
    private final LinkedBlockingQueue<String> resultQ;

    AbstractLinkedBlockingQueuedCFLP(LinkedBlockingQueue<String> resultQ) {
        super();
        this.resultQ = resultQ;
    }

    final protected void queueAndSetSolution(int newUpperBound, int[] newSolution, String label, String jsonResult) {
        try {
            resultQ.put(jsonResult);
            setSolution(newUpperBound, newSolution, label);
        } catch (InterruptedException e) {
            logger.error("Failed to put the following json result into queue\n {}", jsonResult);
            logger.error(e.getLocalizedMessage());
        }
    }

}
