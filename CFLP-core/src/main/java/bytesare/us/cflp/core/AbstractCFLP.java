package bytesare.us.cflp.core;

import java.util.Arrays;
import java.util.function.Supplier;

/**
 * Abstrakte Klasse zum Berechnen der L&ouml;sung mittels Branch-and-Bound.
 *
 */
abstract class AbstractCFLP implements Runnable, Supplier<String> {
	
	/** Die bisher beste L&ouml;sung */
	private BnBSolution bestBnBSolution;

	final public synchronized boolean setSolution(int newUpperBound, int[] newSolution, String label) {
		if (bestBnBSolution == null || newUpperBound < bestBnBSolution.getUpperBound()) {
			bestBnBSolution = new BnBSolution(newUpperBound, newSolution, label);
			return true;
		}
		return false;
	}
	
	/**
	 * Gibt die bisher beste gefundene L&ouml;sung zur&uuml;ck.
	 * 
	 * @return Die bisher beste gefundene L&ouml;sung.
	 */
	final public BnBSolution getBestSolution() {
		return bestBnBSolution;
	}
	
	public final class BnBSolution {

		private final int upperBound;
		private final String label;
		private final int[] mapping;
		
		public BnBSolution(int newUpperBound, int[] newSolution, String label) {
			this.upperBound = newUpperBound;
			this.mapping = newSolution.clone();
			this.label = label;
		}

		/**
		 * @return Die obere Schranke
		 */
		public int getUpperBound() {
			return upperBound;
		}

		/**
		 * @return Die Items der bisher besten L&ouml;sung
		 */
		public int[] getBestSolution() {
			return mapping.clone();
		}

		@Override
		public String toString() {
			return "BnBSolution{" +
					"upperBound=" + upperBound +
					", label='" + label + '\'' +
					", mapping=" + Arrays.toString(mapping) +
					'}';
		}
	}

}
